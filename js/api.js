// Gestion des resultats de requetes 
function ajaxGet(url, callback){

  let req = new XMLHttpRequest();

  req.open("GET",url, true);
  req.addEventListener("load", function(){

    if(req.status >= 200 && req.status < 400){
      callback(req.responseText);
    } else {
      console.error(req.status + " " + req.statusText + " " + url);
    }
  });

  req.addEventListener("error", function(){
    console.error("Erreur reseau avec l'url : " + url);
  })

  req.send(null);
}


let form = document.querySelector("form");

form.addEventListener("submit", function(e){  
  e.preventDefault();

  let ville = form.elements.ville.value;  
  ajaxGet("https://api.openweathermap.org/data/2.5/weather?q=" + ville + "&appid=038bab5310bfeb99ac2d4ff6a20901a8&units=metric", function(reponse){

    tableau = JSON.parse(reponse);
    //console.log(tableau);
  
    let resultat = document.getElementById("resultat");
    let liste = document.createElement("ul");
    liste.id = "liste";
    resultat.innerHTML = "";
    resultat.appendChild(liste);
    
    let listeElt = document.createElement("li");
    listeElt.innerHTML = "Il fait " + tableau.main.temp + "° à " + tableau.name + " en ce moment.";
    document.getElementById("liste").appendChild(listeElt);

  });
});

/*
ajaxGet("https://api.themoviedb.org/3/movie/550?api_key=ae90833ceb596238290cb29f7399317d&language=fr&append_to_response=videos,images", function(reponse){
  let films = JSON.parse(reponse);
  console.log(films);
})
*/